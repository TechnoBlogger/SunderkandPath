package com.shri.raam.bhakt.hanuman.sundarkaand.fragment.nav;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shri.raam.bhakt.hanuman.sundarkaand.R;
import com.shri.raam.bhakt.hanuman.sundarkaand.interfaces.IAllConstants;

/**
 * Created by aman on 18/6/16.
 */
public class FragmentNamedSunderKaand extends Fragment {
    private TextView _textKaand;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_name, null);
        _textKaand = (TextView) rootView.findViewById(R.id.textName);
        _textKaand.setText(IAllConstants.Messages.WHY_NAMED_SUNDERKAAND);
        return rootView;
    }
}
