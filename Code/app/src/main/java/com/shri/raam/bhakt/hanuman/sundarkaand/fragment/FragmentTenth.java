package com.shri.raam.bhakt.hanuman.sundarkaand.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shri.raam.bhakt.hanuman.sundarkaand.R;
import com.shri.raam.bhakt.hanuman.sundarkaand.interfaces.IAllConstants;

/**
 * Created by aman on 18/6/16.
 */
public class FragmentTenth extends Fragment {
    private TextView _textTenth;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_ten, null);
        _textTenth = (TextView) rootView.findViewById(R.id.textTen);
        _textTenth.setText(IAllConstants.Messages.TEN);
        return rootView;
    }
}
