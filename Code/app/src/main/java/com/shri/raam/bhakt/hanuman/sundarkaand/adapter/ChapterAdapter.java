package com.shri.raam.bhakt.hanuman.sundarkaand.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shri.raam.bhakt.hanuman.sundarkaand.R;

import java.util.ArrayList;

/**
 * Created by aman on 20/6/16.
 */

public class ChapterAdapter extends RecyclerView.Adapter<ChapterAdapter.ViewHolder> {
    private ArrayList<String> countries;

    public ChapterAdapter(ArrayList<String> countries) {
        this.countries = countries;
    }


    @Override


    public ChapterAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override


    public void onBindViewHolder(ChapterAdapter.ViewHolder viewHolder, int i) {

        viewHolder.tv_country.setText(countries.get(i));
    }

    @Override

    public int getItemCount() {
        return countries.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_country;

        public ViewHolder(View view) {
            super(view);
            tv_country = (TextView) view.findViewById(R.id.titleChapter);
        }
    }
}