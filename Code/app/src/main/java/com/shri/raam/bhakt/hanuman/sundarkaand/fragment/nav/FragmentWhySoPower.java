package com.shri.raam.bhakt.hanuman.sundarkaand.fragment.nav;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shri.raam.bhakt.hanuman.sundarkaand.R;
import com.shri.raam.bhakt.hanuman.sundarkaand.interfaces.IAllConstants;

/**
 * Created by aman on 18/6/16.
 */
public class FragmentWhySoPower extends Fragment {
    private TextView _textPower;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_power, null);
        _textPower = (TextView) rootView.findViewById(R.id.textPower);
        _textPower.setText(IAllConstants.Messages.WHY_IT_IS_SUPPOSED_TO_HAVE_POWER);

        return rootView;
    }
}
