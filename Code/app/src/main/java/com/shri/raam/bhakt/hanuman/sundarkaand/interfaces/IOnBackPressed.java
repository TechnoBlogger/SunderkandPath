package com.shri.raam.bhakt.hanuman.sundarkaand.interfaces;


/**
 * Created by TechnoBlogger on 27/1/16.
 */

public interface IOnBackPressed {
	public boolean onBackPressed();

}
