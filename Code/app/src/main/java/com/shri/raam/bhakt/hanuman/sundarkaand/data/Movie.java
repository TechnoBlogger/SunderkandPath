package com.shri.raam.bhakt.hanuman.sundarkaand.data;

/**
 * Created by aman on 20/6/16.
 */
public class Movie {
    private String title;

    public Movie() {
    }

    public Movie(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }
}