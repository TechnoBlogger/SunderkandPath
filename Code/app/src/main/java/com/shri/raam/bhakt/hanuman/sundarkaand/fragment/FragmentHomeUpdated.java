package com.shri.raam.bhakt.hanuman.sundarkaand.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.shri.raam.bhakt.hanuman.sundarkaand.R;
import com.shri.raam.bhakt.hanuman.sundarkaand.SingletonApp;
import com.shri.raam.bhakt.hanuman.sundarkaand.adapter.ChapterAdapter;
import com.shri.raam.bhakt.hanuman.sundarkaand.interfaces.IAllConstants;

import java.util.ArrayList;

/**
 * Created by aman on 20/6/16.
 */
public class FragmentHomeUpdated extends Fragment {
    private ArrayList<String> chapterTitle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, null);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(SingletonApp.getApp().getFragmentActivityInstance());
        recyclerView.setLayoutManager(layoutManager);
        chapterTitle = new ArrayList<>();
        chapterTitle.add(IAllConstants.Messages.CHAPTER_01);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_02);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_03);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_04);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_05);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_06);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_07);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_08);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_09);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_10);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_11);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_12);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_13);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_14);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_15);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_16);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_17);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_18);
        chapterTitle.add(IAllConstants.Messages.CHAPTER_19);


        RecyclerView.Adapter adapter = new ChapterAdapter(chapterTitle);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(SingletonApp.getApp().getFragmentActivityInstance(), recyclerView, new ClickListener() {

            @Override
            public void onClick(View view, int position) {
                chapterTitle.get(position);

                switch (position) {
                    case 0:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentOne(), true);
                        break;
                    case 1:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentTwo(), true);
                        break;
                    case 2:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentThree(), true);
                        break;
                    case 3:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentFour(), true);
                        break;
                    case 4:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentFive(), true);
                        break;
                    case 5:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentSix(), true);
                        break;
                    case 6:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentSeven(), true);
                        break;
                    case 7:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentEight(), true);
                        break;
                    case 8:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentNine(), true);
                        break;
                    case 9:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentTenth(), true);
                        break;
                    case 10:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentEleventh(), true);
                        break;
                    case 11:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentTwelve(), true);
                        break;
                    case 12:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentThirteen(), true);
                        break;
                    case 13:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentFourteen(), true);
                        break;
                    case 14:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentFifeteen(), true);
                        break;
                    case 15:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentSixteen(), true);
                        break;
                    case 16:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentSeventeen(), true);
                        break;
                    case 17:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentEighteen(), true);
                        break;
                    case 18:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentNineteen(), true);
                        break;


                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
/*

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(SingletonApp.getApp().getFragmentActivityInstance(), new RecyclerItemClickListener.OnItemClickListener() {

            @Override
            public void onClick(View view, int position) {
                chapterTitle.get(position);

                switch (position) {
                    case 0:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentOne(), true);
                        break;
                    case 1:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentTwo(), true);
                        break;
                    case 2:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentThree(), true);
                        break;
                    case 3:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentFour(), true);
                        break;
                    case 4:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentFive(), true);
                        break;
                    case 5:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentSix(), true);
                        break;
                    case 6:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentSeven(), true);
                        break;
                    case 7:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentEight(), true);
                        break;
                    case 8:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentNine(), true);
                        break;
                    case 9:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentTenth(), true);
                        break;
                    case 10:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentEleventh(), true);
                        break;
                    case 11:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentTwelve(), true);
                        break;
                    case 12:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentThirteen(), true);
                        break;
                    case 13:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentFourteen(), true);
                        break;
                    case 14:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentFifeteen(), true);
                        break;
                    case 15:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentSixteen(), true);
                        break;
                    case 16:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentSeventeen(), true);
                        break;
                    case 17:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentEighteen(), true);
                        break;
                    case 18:
                        SingletonApp.getApp().getFlowInstanse().replace(new FragmentNineteen(), true);
                        break;


                }
            }


        }));

*/

        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            GestureDetector gestureDetector = new GestureDetector(SingletonApp.getApp().getFragmentActivityInstance(), new GestureDetector.SimpleOnGestureListener() {

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if (child != null && gestureDetector.onTouchEvent(e)) {
                    int position = rv.getChildAdapterPosition(child);
                }

                return false;
            }


            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        return rootView;
    }

/*
    public class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
        private AdapterView.OnItemClickListener mListener;



        GestureDetector mGestureDetector;

        public RecyclerItemClickListener(Context context, AdapterView.OnItemClickListener listener) {
            mListener = listener;
            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }
            });
        }

        @Override public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
            View childView = view.findChildViewUnder(e.getX(), e.getY());
            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                return true;
            }
            return false;
        }

        @Override public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) { }

        @Override
        public void onRequestDisallowInterceptTouchEvent (boolean disallowIntercept){}
    }*/

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private FragmentHomeUpdated.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final FragmentHomeUpdated.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}

