package com.shri.raam.bhakt.hanuman.sundarkaand.fragment.nav;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shri.raam.bhakt.hanuman.sundarkaand.R;

/**
 * Created by aman on 18/6/16.
 */
public class FragmentAboutSunderKaand extends Fragment {
    TextView _textAbout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about, null);
        _textAbout = (TextView) rootView.findViewById(R.id.textAbout);
        //_textAbout.setText(IAllConstants.Messages.ABOUT);
        return rootView;
    }
}
