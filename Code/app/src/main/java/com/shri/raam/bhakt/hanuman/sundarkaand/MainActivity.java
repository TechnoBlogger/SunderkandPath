package com.shri.raam.bhakt.hanuman.sundarkaand;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.appbrain.AppBrain;
import com.appbrain.InterstitialBuilder;
import com.shri.raam.bhakt.hanuman.sundarkaand.fragment.FragmentHomeUpdated;
import com.shri.raam.bhakt.hanuman.sundarkaand.fragment.nav.FragmentNamedSunderKaand;
import com.shri.raam.bhakt.hanuman.sundarkaand.fragment.nav.FragmentWhatIsSunderKaand;
import com.shri.raam.bhakt.hanuman.sundarkaand.fragment.nav.FragmentWhatItContains;
import com.shri.raam.bhakt.hanuman.sundarkaand.fragment.nav.FragmentWhySoPower;
import com.shri.raam.bhakt.hanuman.sundarkaand.interfaces.IAllConstants;

import java.util.List;

public class MainActivity extends FragmentActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppBrain.init(this);
        if (savedInstanceState == null) {
            InterstitialBuilder.create().show(this);
        }
        SingletonApp.getApp().initActivity(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(IAllConstants.Messages.TITLE);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareYourApp();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        SingletonApp.getApp().getFlowInstanse().replace(new FragmentHomeUpdated(), false);
    }

    public void shareYourApp() {
        try {
            final String appPackageName = getPackageName();
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Sundar Kaand");
            String sAux = "\nLet me recommend you this application\n\n";
            sAux = sAux + ("https://play.google.com/store/apps/details?id=" + appPackageName);
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "Select ONE"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (getSupportFragmentManager() != null) {
            List<Fragment> list = getSupportFragmentManager().getFragments();
            if (list == null)
                return;
            for (Fragment f : list) {
                if (f != null)
                    f.onLowMemory();
            }
        }
    }


    @Override
    protected void onDestroy() {
        SingletonApp.getApp().onDestroy();
        super.onDestroy();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_about) {
            SingletonApp.getApp().getFlowInstanse().replace(new FragmentHomeUpdated(), false);
            SingletonApp.getApp().getFlowInstanse().clearBackStack();
        } else if (id == R.id.nav_what) {
            SingletonApp.getApp().getFlowInstanse().replace(new FragmentWhatIsSunderKaand(), true);
        } else if (id == R.id.nav_contain) {
            SingletonApp.getApp().getFlowInstanse().replace(new FragmentWhatItContains(), true);
        } else if (id == R.id.nav_power) {
            SingletonApp.getApp().getFlowInstanse().replace(new FragmentWhySoPower(), true);
        } else if (id == R.id.nav_whynamed) {
            SingletonApp.getApp().getFlowInstanse().replace(new FragmentNamedSunderKaand(), true);
        } else if (id == R.id.nav_share) {
            shareYourApp();
        } else if (id == R.id.nav_rate) {
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.show();
            final String appPackageName = getPackageName();
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri
                        .parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id="
                                + appPackageName)));
            }
            dialog.dismiss();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
